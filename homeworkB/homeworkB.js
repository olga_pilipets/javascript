'use strict'
function countVowel(string) {

    let count = 0;
    let regexp = new RegExp(/[аоуэиыеёяю]/i);

    for (let char of string) {
        if (regexp.test(char)) {
            count++;
        }
    }
    return count;

}
(function checkString() {
    let string = prompt("Введите строку");
    while (true) {
        if (string==""||string == null) {
            string = prompt("Введите строку");
        } else {
            break;
        }
    }
    console.log("Количество кириллических гласных в строке "+"\""+string+"\""+" :" + countVowel(string));

})();

