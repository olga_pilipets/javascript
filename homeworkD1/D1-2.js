function countVowel(string) {

    var count = 0;
    var regexp = new RegExp(/[аоуэиыеёяю]/i);
    var array = string.split('');
    var vowelArray = array.filter((x) => {
        return regexp.test(x)
    });
    vowelArray.forEach(() => {
        count++
    })
    return count;

}

(function checkString() {
    var string;
    do {
        string = prompt("Введите строку");
    } while (string == "" || string == null)

    console.log("Количество кириллических гласных в строке " + "\"" + string + "\"" + " :" + countVowel(string));

})();

