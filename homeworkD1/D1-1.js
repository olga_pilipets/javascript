
function countVowel(string) {

    var count = 0;
    var regexp = new RegExp(/[аоуэиыеёяю]/i);
    var array = string.split('');
    array.forEach((x) => {
        if (regexp.test(x)) {
            count++;
        }
    })

    return count;

}
(function checkString() {
    var string;
    do {
        string = prompt("Введите строку");
    } while (string == "" || string == null)

    console.log("Количество кириллических гласных в строке " + "\"" + string + "\"" + " :" + countVowel(string));

})();


