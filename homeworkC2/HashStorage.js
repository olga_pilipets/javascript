class HashStorage {
    constructor() {
        this._drinks = {};
    }

    addValue(key, value) {
        this._drinks[key] = value;
    }

    getValue(key) {
        if (key in this._drinks) {
            return this._drinks[key];
        }
        else {
            return undefined;
        }
    }

    deleteValue(key) {
        if (key in this._drinks ) {
            delete this._drinks[key]
            return true;
        }
        else {
            return false;
        }
    }

    getKeys() {
        var keys = [];
        for (var k in this._drinks) {
            keys.push(k);
        }
        return keys;
    }
}
function addInfo(message) {
    var info;
    do {
        info = prompt(message);
    } while (info == null || info == "")
    return info;
}

var drinkStorage = new HashStorage();

function addDrinkInfo() {
    var name = addInfo("Введите название напитка");
    var receipt = addInfo("Введите рецепт приготовления напитка");
    var alcohol = confirm("Напиток алкогольный?");
    if(alcohol){
        alcohol="да";
    }
    else {
        alcohol="нет";
    }
    drinkStorage.addValue(name, [alcohol, receipt]);
}

function getDrinkInfo() {
    var drinkName = addInfo("Введите название напитка для поиска");
    var drinkInfo = drinkStorage.getValue(drinkName);
    if (drinkInfo !== undefined) {
        alert("напиток: " + drinkName + "\n" + "алкогольный: " + drinkInfo[0] + "\n" + "рецепт приготовления: " + drinkInfo[1]);
    }
    else {
        alert("такого напитка нет")
    }
}
function deleteDrinkInfo() {
    var drinkName = addInfo("Введите название напитка для удаления");
    if(drinkStorage.deleteValue(drinkName)){
        alert("удалено");
    } else {
        alert("удаление невозможно");
    }

}
function getListOfDrinks() {
    var listOfDrinks = drinkStorage.getKeys();
    var list = "";

    function add(v, i, a) {
        list += v + "\n";
    }

    listOfDrinks.forEach(add);
    if (list === "") {
        alert("перечень напитков пуст");
    } else {
        alert(list);
    }
}