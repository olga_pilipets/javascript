/*Сгенерировать массив из N случайных целых чисел. Поменять местами все элементы относительно середины массива по следующей схеме:
 Было [1,2,3,4,5,6,7], стало [5,6,7,4,1,2,3]. Создавать новые массивы нельзя.*/
function randomArraySorting(n) {
    if (n<=0||n!==n||n===null||n===undefined) {
        console.log("array should consist of 1+ elements")
    }
    else {
        var array = [];
        for (var i = 0; i < n; i++) {
            array[i] = Math.floor(Math.random() * 15)
        }
        console.log(array);
        var length = array.length;
        var half = parseInt(length / 2);
        var storage;
        for (var i = 0; i < half; i++) {
            if (length % 2 == 0) {
                storage = array[i];
                array[i] = array[i + half];
                array[i + half] = storage;
            } else {
                storage = array[i];
                array[i] = array[i + half + 1];
                array[i + half + 1] = storage;
            }
        }
        console.log(array);
    }
}
randomArraySorting(20);
randomArraySorting(21);
randomArraySorting(0);
randomArraySorting();
randomArraySorting(-1);
randomArraySorting("-1");