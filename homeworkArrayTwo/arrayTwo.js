/**
 * Created by point on 16.11.2017.
 */
/*Сгенерировать массив из N случайных целых чисел. Поменять местами все элементы относительно середины массива по следующей схеме:
 Было [1,2,3,4,5,6,7,8,9], стало [4,3,2,1,5,9,8,7,6]. Создавать новые массивы нельзя. */

function randomArraySorting(n) {
    if (n <= 0 || n !== n || n === null || n === undefined) {
        console.log("array should consist of 1+ elements")
    }
    else {
        var array = [];
        for (var i = 0; i < n; i++) {
            array[i] = Math.floor(Math.random() * 15)
        }
        console.log(array);
        var length = array.length;
        var half = Math.floor(length / 2);
        var quarter = Math.floor(half / 2);
        var rest = (length % 2);
        for (var j = 0; j < quarter; j++) {
            var lastIndexLeft = half - 1 - i;
            var current = array[i];
            array[i] = array[lastIndexLeft];
            array[lastIndexLeft] = current;
            var lastIndexRight = length - 1 - i;
            var firstIndexRight = half + rest + i;
            current = array[firstIndexRight];
            array[firstIndexRight] = array[lastIndexRight];
            array[lastIndexRight] = current;
        }
        return array;
    }

    console.log(array);


}
randomArraySorting(20);
randomArraySorting(21);
randomArraySorting(0);
randomArraySorting();
randomArraySorting(-1);
randomArraySorting("-1");


