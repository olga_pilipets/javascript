'use strict'

var readline = require('readline');
const info = {
    surname: "",
    name: "",
    fathername: "",
    age: 0,
    gender: "",
    majorityStatus: "",
    retirementStatus: ""

};

function getSurname() {

    const rl = prompt();

    rl.question("Введите фамилию: ", (answer) => {
        rl.close();
        if (answer == null || answer == "") {
            getSurname();
        } else {
            info.surname = answer;
            return getName();
        }
    });

}
function getName() {

    const rl = prompt();

    rl.question("Введите имя: ", (answer) => {
        rl.close();
        if (answer == null || answer == "") {
            getName();
        } else {
            info.name = answer;
            return getFathername();
        }
    });

}
function getFathername() {

    const rl = prompt();

    rl.question("Введите отчество: ", (answer) => {
        rl.close();
        if (answer == null || answer == "") {
            getFathername();
        } else {
            info.fathername = answer;
            return getAge();
        }
    });

}
function getAge() {

    const rl = prompt();

    rl.question("Введите возраст (в годах): ", (answer) => {
        rl.close();
        if (isNaN(answer) || answer <= 0 || answer > 125) {
            getAge();
        } else {
            info.age = answer;
            if (info.age >= 18) {
                info.majorityStatus = "Вы уже совершеннолетний";
            } else {
                info.majorityStatus = "Вы не совершеннолетний";
            }
            if (info.age >= 60) {
                info.retirementStatus = "да";
            } else {
                info.retirementStatus = "нет";
            }
            return getGender();
        }
    });

}
function getGender() {

    const rl = prompt();

    rl.question("Вы женщина?(да/нет): ", (answer) => {

        rl.close();
        if (answer === "да" || answer === "нет") {
            info.gender = "женский";
            showInfo();
        }
        else if (answer === "нет") {
            info.gender = "мужской";
            showInfo();

        } else {
            getGender();
        }
    });
}

function showInfo() {

    console.log("Ваше ФИО: " + info.surname + " " + info.name + " " + info.fathername + "\n" +
        "Ваш возраст в годах: " + info.age + "\n" +
        "Ваш возраст в днях: " + info.age * 365 + "\n" +
        info.majorityStatus + "\n" +
        "Ваш пол: " + info.gender + "\n" +
        "Вы на пенсии: " + info.retirementStatus);
}
function prompt() {
    return readline.createInterface({input: process.stdin, output: process.stdout});
}
getSurname();

